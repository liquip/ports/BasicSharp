using System;
using System.Collections.Generic;
using System.Text;
using BasicSharp;
using Sys = Cosmos.System;


namespace SharpZipLib.Test;

public class Kernel : Sys.Kernel
{


    public string ForLoop = $@"
let a = 0

For i = 1 To 10
    let a = a + 1
Next i

assert a = 10

For i = 10 To 9
assert 0
Next i
    ";

    public string GoTo = $@"
GoTo abc
assert 0

def:
assert 1
End

abc:
assert 1

GoTo def
assert 0
    ";

    protected override void Run()
    {

        var forloop = new Interpreter(ForLoop);
        forloop.Exec();

        var goTo = new Interpreter(GoTo);
        goTo.Exec();

    }
}
